package net.gotech.family.login;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

public class FamilyHttpUtils {

	private static final String AUTH = "Authorization";
	
	
	public ResponseEntity<JsonNode> httpRequest(String url, HttpMethod method, Object request_body, String auth) throws IOException{	
		RestTemplate restTemplate = new RestTemplate();
//		restTemplate.setErrorHandler(new FamilyErrorHandler());
		
		
	    HttpHeaders headers = new HttpHeaders();
	    
//	    setAuthHeader(headers, auth);
	    
	    headers.set("Content-Type", "application/json");
	    HttpEntity<Object> entity = new HttpEntity<>(request_body, headers);
	    
	    HttpClient httpClient = HttpClients.createDefault();
	    restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient)); 
	    
	    
	    
	    ResponseEntity<JsonNode> response = restTemplate.exchange(url, method, entity, JsonNode.class);
	    
		if(response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			System.out.println(url);
			System.out.println(auth);
			System.out.println("UNAUTHORIZED :: "+response);
//			throw new AccessDeniedException();
		}
		return response;
	}
	
	private String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
	private String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                urlEncodeUTF8(entry.getKey().toString()),
                urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();       
    }
	
}
