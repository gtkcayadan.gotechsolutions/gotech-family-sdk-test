package net.gotech.family.login;

import com.fasterxml.jackson.databind.JsonNode;

public interface FamilyLoginService {

	public JsonNode authenticate(JsonNode jsonNode);
}
