package net.gotech.family.login.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.cloud.sdk.exception.ResourceNotFoundException;
import net.gotech.family.cloud.sdk.jwt.AuthenticationRequest;

@Aspect
@Component
public class FamilyLoginServiceAspect {
	
	@Autowired
	private AuthenticationRequest authenticationRequest; 
	
	@Around("execution(* FamilyLoginServiceImpl.authenticate(..))")
	public Object catchMethod(ProceedingJoinPoint point) throws Throwable {
		System.out.println("Before invoking getName() method");
		JsonNode value = null;
		try {
			value = (JsonNode) point.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		if (value == null) {
			throw new ResourceNotFoundException("Invalid account");
		}
		System.out.println("After invoking getName() method. Return value="+value);
		
		long organizationId = value.get("organizationId") != null ? value.get("organizationId").asLong() : 0;
		boolean isForceChangePass = value.get("isForceChangePass") != null ? value.get("isForceChangePass").asBoolean() : false;
		
		ObjectNode object = (ObjectNode) value;
		object.remove("organizationId");
		object.remove("isForceChangePass");
		System.err.println(value);
		
		ResponseEntity<JsonNode> responseEntity = authenticationRequest.getToken(value);
		
		if(responseEntity.getStatusCode().is2xxSuccessful()) {
			System.err.println(responseEntity.getHeaders().get("Authorization").get(0));
			((ObjectNode)value).put("authorization", responseEntity.getHeaders().get("Authorization").get(0));
		}
		object.put("organizationId", organizationId);
		object.put("isForceChangePass", isForceChangePass);
		return value;
	}
	
}
