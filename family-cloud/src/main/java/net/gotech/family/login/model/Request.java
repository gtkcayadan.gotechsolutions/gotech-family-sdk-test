package net.gotech.family.login.model;

import java.util.Map;

public class Request {
	
	private long id;
	private String username;
	private String type;
	
	private Map<String, Object> obj;
	
	public Request(long id, String username, String type) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.username=username;
		this.type=type;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<String, Object> getObj() {
		return obj;
	}
	public void setObj(Map<String, Object> obj) {
		this.obj = obj;
	}

	@Override
	public String toString() {
		return "Request [id=" + id + ", username=" + username + ", type=" + type + ", obj=" + obj + "]";
	}
}
