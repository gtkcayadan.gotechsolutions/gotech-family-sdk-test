package net.gotech.family.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping(value = "/Api/Login")
public class LoginRestController {

	@Autowired
	private FamilyLoginService familyLoginService;
	
	@RequestMapping(value= "", method=RequestMethod.POST, produces="application/json")
	public JsonNode login(@RequestBody JsonNode node) {
		return familyLoginService.authenticate(node);
	}
	
}
