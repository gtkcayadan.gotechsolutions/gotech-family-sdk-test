

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import net.gotech.family.table.filter.DataType;
import net.gotech.family.table.filter.Value;

public enum NotificationFilter {

	NAME("name", new Value("name", DataType.STRING))
	
	;
	
	private String key;
	private Value value;
	
	NotificationFilter(String key, Value value){
		this.key=key;
		this.value=value;
	}
	
	public static Map<String, Value> getMap(){
		return Arrays.stream(NotificationFilter.values())
			      .collect(Collectors.toMap(NotificationFilter::getKey, NotificationFilter::getValue));
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}
}
