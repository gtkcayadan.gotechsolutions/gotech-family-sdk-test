package net.gotech.family.table.dyanamic.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class SystemLogContent {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	@OneToOne
	private SystemLog systemLog;
	private String logField;
	private String logValue;
	
	public SystemLogContent() {
	}
	
	public SystemLogContent(String logField, String logValue, SystemLog systemLog) {
		this.logField = logField;
		this.logValue = logValue;
		this.systemLog = systemLog;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public SystemLog getSystemLog() {
		return systemLog;
	}
	public void setSystemLog(SystemLog systemLog) {
		this.systemLog = systemLog;
	}
	public String getLogField() {
		return logField;
	}
	public void setLogField(String logField) {
		this.logField = logField;
	}
	public String getLogValue() {
		return logValue;
	}
	public void setLogValue(String logValue) {
		this.logValue = logValue;
	}
	@Override
	public String toString() {
		return "SystemLogContent [id=" + id + ", systemLog=" + systemLog + ", logField=" + logField + ", logValue="
				+ logValue + "]";
	}
	
}
