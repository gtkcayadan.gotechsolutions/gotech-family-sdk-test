package net.gotech.family.table.dyanamic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import net.gotech.family.table.dyanamic.entity.TableSelect.TableSelectEnums;
import net.gotech.family.table.dyanamic.service.TableSelectService;


@EnableAutoConfiguration
@RestController
@RequestMapping(value= {"/TableSelect"})
public class TableSelectRestController {
	
	@Autowired
	private TableSelectService tableSelectService;

	@RequestMapping(value= "/{id}", method=RequestMethod.GET, produces="application/json")
	public JsonNode getTableFields(@PathVariable long id, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "type", required = true) TableSelectEnums type){
		return tableSelectService.getFieldsByAccountAndName(id, name, type);
	}
	
	@RequestMapping(value= "/{id}/Type", method=RequestMethod.GET, produces="application/json")
	public JsonNode getTableFields(@PathVariable long id, @RequestParam(value = "type", required = true) TableSelectEnums type){
		return tableSelectService.getFieldsByAccountAndType(id, type);
	}
	
	@RequestMapping(value= "/{id}", method=RequestMethod.PUT, produces="application/json")
	public JsonNode addTableFields(@PathVariable long id, @RequestBody JsonNode params){
		return tableSelectService.createAccountField(id, params, TableSelectEnums.valueOf(params.get("type").asText()));
	}
	
	@RequestMapping(value= "/{id}", method=RequestMethod.DELETE, produces="application/json")
	public JsonNode deleteTableFields(@PathVariable long id, @RequestBody JsonNode params){
		return tableSelectService.deleteAccountField(id, params, TableSelectEnums.valueOf(params.get("type").asText()));
	}
	
	@RequestMapping(value= "/{id}", method=RequestMethod.POST, produces="application/json")
	public JsonNode updateTableFields(@PathVariable long id, @RequestBody JsonNode params){
		return tableSelectService.updateAccountField(id, params, TableSelectEnums.valueOf(params.get("type").asText()));
	}
	
}
