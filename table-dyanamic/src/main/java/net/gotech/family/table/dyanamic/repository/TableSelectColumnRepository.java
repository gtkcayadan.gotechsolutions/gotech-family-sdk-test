package net.gotech.family.table.dyanamic.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.gotech.family.table.dyanamic.entity.TableSelect;
import net.gotech.family.table.dyanamic.entity.TableSelectColumn;

@Repository
public interface TableSelectColumnRepository extends CrudRepository<TableSelectColumn, Long>{

	Optional<TableSelectColumn> findByAccountIdAndTableSelect(long id, TableSelect tableSelect);

	Optional<TableSelectColumn> findByAccountId(long accountId);

}
