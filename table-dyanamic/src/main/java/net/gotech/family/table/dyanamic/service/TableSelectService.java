package net.gotech.family.table.dyanamic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.table.dyanamic.entity.TableSelect;
import net.gotech.family.table.dyanamic.entity.TableSelectColumn;
import net.gotech.family.table.dyanamic.entity.TableSelect.TableSelectEnums;
import net.gotech.family.table.dyanamic.repository.TableSelectColumnRepository;
import net.gotech.family.table.dyanamic.repository.TableSelectRepository;

@Service
public class TableSelectService {
	
	public TableSelectService() {
		System.out.println("TableSelectService initialized...");
	}
	
	@Autowired
	private TableSelectColumnRepository tableSelectColumnRepository;
	@Autowired
	private TableSelectRepository tableSelectRepository;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	public JsonNode getFieldsByAccountAndName(long accountId, String name, TableSelectEnums type) {
		System.out.println(accountId + " " +name);
		if(accountId != 0 && name != null) {
			Optional<TableSelect> tableSelect = tableSelectRepository.findByNameAndType(name, type);
			if(tableSelect.isPresent()) {
				Optional<TableSelectColumn> tableSelectColumn = tableSelectColumnRepository.findByAccountIdAndTableSelect(accountId, tableSelect.get());
				if(tableSelectColumn.isPresent()) {
					return tableSelectColumn.get().toJson(objectMapper);
				}
			}
		}else if(accountId != 0){
			return getFieldsByAccount(accountId);
		}
		return objectMapper.createObjectNode();
	}
	
	private JsonNode getFieldsByAccount(long accountId) {
		Optional<TableSelectColumn> tableSelectColumn = tableSelectColumnRepository.findByAccountId(accountId);
		if(tableSelectColumn.isPresent()) {
			return tableSelectColumn.get().toJson(objectMapper);
		}
		return objectMapper.createObjectNode();
	}

	public JsonNode createAccountField(long accountId, JsonNode params, TableSelectEnums type) {
		Optional<TableSelect> tableSelect = tableSelectRepository.findByNameAndType(params.get("name").asText(), type);
		TableSelect select = null;
		if (tableSelect.isPresent()) {
			select = tableSelect.get();
		}else {
			select = new TableSelect();
			select.setName(params.get("name").asText());
			select.setType(type);
			select = tableSelectRepository.save(select);
		}
		
		TableSelectColumn selectColumn = new TableSelectColumn();
		selectColumn.setAccountId(accountId);
		selectColumn.setField(params.get("field").asText());
		selectColumn.setTableSelect(select);
		
		selectColumn = tableSelectColumnRepository.save(selectColumn);
		
		return selectColumn.toJson(objectMapper);
	}

	public JsonNode deleteAccountField(long accountId, JsonNode params, TableSelectEnums type) {
		Optional<TableSelect> tableSelect = tableSelectRepository.findByNameAndType(params.get("name").asText(), type);
		if (tableSelect.isPresent()) {
			Optional<TableSelectColumn> optional = tableSelectColumnRepository.findByAccountIdAndTableSelect(accountId, tableSelect.get());
			if(optional.isPresent()) {
				return optional.get().toJson(objectMapper);
			}
		}
		return objectMapper.createObjectNode();
	}

	public JsonNode updateAccountField(long accountId, JsonNode params, TableSelectEnums type) {
		
		Optional<TableSelect> tableSelect = tableSelectRepository.findByNameAndType(params.get("name").asText(), type);
		
		
		System.out.println("tableSelect " +tableSelect.isPresent());
		
		TableSelect select = null;
		if (tableSelect.isPresent()) {
			select = tableSelect.get();
		}else {
			select = new TableSelect();
			select.setType(type);
			select.setName(params.get("name").asText());
			select = tableSelectRepository.save(select);
		}
		
		System.out.println(tableSelectColumnRepository);
		
		Optional<TableSelectColumn> optional = tableSelectColumnRepository.findByAccountIdAndTableSelect(accountId, select);
		TableSelectColumn selectColumn = null;
		if(optional.isPresent()) {
			selectColumn = optional.get();
		}else {
			selectColumn = new TableSelectColumn();
		}
		selectColumn.setAccountId(accountId);
		selectColumn.setField(params.get("field").asText());
		selectColumn.setTableSelect(select);
		
		selectColumn = tableSelectColumnRepository.save(selectColumn);
		
		return selectColumn.toJson(objectMapper);
	}

	public JsonNode getFieldsByAccountAndType(long accountId, TableSelectEnums type) {
		ArrayNode nodes = objectMapper.createArrayNode();
		if(accountId != 0 && type != null) {
			List<TableSelect> tableSelect = tableSelectRepository.findAllByType(type);
			
			System.out.println(tableSelect);
			
			for (TableSelect tableSelect2 : tableSelect) {
//				ObjectNode node = (ObjectNode) tableSelect2.toJson(objectMapper);
				Optional<TableSelectColumn> tableSelectColumn = tableSelectColumnRepository.findByAccountIdAndTableSelect(accountId, tableSelect2);
				if(tableSelectColumn.isPresent()) {
					nodes.add(tableSelectColumn.get().toJson(objectMapper));
				}
			}
			
			
			
//			if(tableSelect.isPresent()) {
//				Optional<TableSelectColumn> tableSelectColumn = tableSelectColumnRepository.findByAccountIdAndTableSelect(accountId, tableSelect.get());
//				if(tableSelectColumn.isPresent()) {
//					return tableSelectColumn.get().toJson(objectMapper);
//				}
//			}
		}
		return nodes;
	}
	
}
