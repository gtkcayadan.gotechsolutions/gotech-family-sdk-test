package net.gotech.family.table.dyanamic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import net.gotech.family.table.dyanamic.entity.TableSelect;
import net.gotech.family.table.dyanamic.entity.TableSelect.TableSelectEnums;

@Component("tableSelectRepository")
public interface TableSelectRepository extends JpaRepository<TableSelect, Long>{

	Optional<TableSelect> findByNameAndType(String name, TableSelectEnums type);

	List<TableSelect> findAllByType(TableSelectEnums type);

}
