package net.gotech.family.table.dyanamic.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.table.dyanamic.entity.SystemLog;
import net.gotech.family.table.dyanamic.entity.SystemLogContent;
import net.gotech.family.table.dyanamic.repository.SystemLogContentRepository;
import net.gotech.family.table.dyanamic.repository.SystemLogRepository;

@Service
public class SystemLogService {

	@Autowired
	private SystemLogRepository systemLogRepository;
	
	@Autowired
	private SystemLogContentRepository systemLogContentRepository;
	
	public JsonNode findSystemLogs() {
		return null;
	}
	
	private SystemLog systemLog;
	
	
	public List<SystemLogContent> save(JsonNode node) {
		systemLog = new SystemLog();
		systemLog.setTimestamp(new Timestamp(System.currentTimeMillis()));
		
		systemLog = systemLogRepository.save(systemLog);
		
		List<SystemLogContent> contents = new ArrayList<SystemLogContent>();
		node.fieldNames().forEachRemaining(data -> {
			contents.add(new SystemLogContent(data.toString(), node.get(data).asText(), systemLog));
		});
		
		return systemLogContentRepository.saveAll(contents);
	}
	
	public static void main(String[] args) {
		ObjectNode jsonNode = new ObjectMapper().createObjectNode();
		jsonNode.put("te",1);
		jsonNode.put("t2",2);
		jsonNode.put("t3",3);
		
		
	}
}
