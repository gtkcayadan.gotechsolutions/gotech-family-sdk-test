package net.gotech.family.table.dyanamic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.gotech.family.table.dyanamic.entity.SystemLog;

public interface SystemLogRepository extends JpaRepository<SystemLog, Long>{

}
