package net.gotech.family.table.dyanamic.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class SystemLog {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private Timestamp timestamp;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public String toString() {
		return "SystemLog [id=" + id + ", timestamp=" + timestamp + "]";
	}
}
