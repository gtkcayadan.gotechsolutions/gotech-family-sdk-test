package net.gotech.family.table.dyanamic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Entity
@Table
public class TableSelectColumn implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private long accountId;
	@Column(length=1024)
	private String field;
	@OneToOne(fetch=FetchType.EAGER)
	private TableSelect tableSelect;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public TableSelect getTableSelect() {
		return tableSelect;
	}
	public void setTableSelect(TableSelect tableSelect) {
		this.tableSelect = tableSelect;
	}
	
	public JsonNode toJson(ObjectMapper objectMapper) {
		ObjectNode node = objectMapper.createObjectNode();
		node.put("field", getField());
		node.put("table", getTableSelect().getName());
		return node;
	}
	
	@Override
	public String toString() {
		return "TableSelectColumn [id=" + id + ", accoundId=" + accountId + ", field=" + field + ", tableSelect="
				+ tableSelect + "]";
	}
}
