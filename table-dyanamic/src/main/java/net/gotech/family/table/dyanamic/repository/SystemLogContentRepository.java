package net.gotech.family.table.dyanamic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.gotech.family.table.dyanamic.entity.SystemLog;
import net.gotech.family.table.dyanamic.entity.SystemLogContent;

public interface SystemLogContentRepository extends JpaRepository<SystemLogContent, Long>{

}
