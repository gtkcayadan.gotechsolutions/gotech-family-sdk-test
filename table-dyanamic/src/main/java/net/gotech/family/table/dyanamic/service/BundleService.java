package net.gotech.family.table.dyanamic.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.gotech.family.table.filter.DataType;
import net.gotech.family.table.filter.TableParam;
import net.gotech.family.table.filter.Value;

public abstract class BundleService {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}
	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
	public abstract JSONObject findAll(TableParam params) throws Exception;
	
	public abstract long count(String filter, Object[] params);
	
	public RowMapper<JSONObject> toJsonMapper(Map<String, Value> map){
		return new RowMapper<JSONObject>() {

	        @Override
	        public JSONObject mapRow(ResultSet rs, int arg1) throws SQLException {
	        	JSONObject user = new JSONObject();
	        	
	        	for (Entry<String, Value> entry : map.entrySet()) {
//	        		System.out.println(entry.getValue().getType());
	        	    try {
	        	    	if(entry.getValue().getType() == DataType.JSON_ARRAY) {
	        	    		JSONArray jsonobj = new JSONArray(rs.getString(entry.getValue().getColumn()));
	        	    		user.put(entry.getKey(), jsonobj);
	        	    	}else if(entry.getValue().getType() == DataType.BOOLEAN) {
	        	    		user.put(entry.getKey(), rs.getBoolean(entry.getValue().getColumn()));
	        	    	}else
	        	    		user.put(entry.getKey(), rs.getString(entry.getValue().getColumn()));
					} catch (Exception e) {
//						System.err.println(e.getMessage());
					}
	        	}
	        	
	        	try {
	        		user.put("id", rs.getLong("id"));
				} catch (Exception e) {
				}
	            return user;
	        }
	    };
	}
	
}
