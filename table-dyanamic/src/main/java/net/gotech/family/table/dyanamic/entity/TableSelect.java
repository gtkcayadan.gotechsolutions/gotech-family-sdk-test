package net.gotech.family.table.dyanamic.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Entity
@Table
public class TableSelect {
	
	public enum TableSelectEnums{TABLE, FILTER, NONE}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String name;
	@Enumerated(EnumType.ORDINAL)
	private TableSelectEnums type;
	
	public TableSelectEnums getType() {
		return type;
	}
	public void setType(TableSelectEnums type) {
		this.type = type;
	}
	private boolean status;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TableSelect [id=" + id + ", name=" + name + ", type=" + type + ", status=" + status + "]";
	}
	
	public JsonNode toJson(ObjectMapper objectMapper) {
		ObjectNode node = objectMapper.createObjectNode();
		node.put("id", getId());
		node.put("name", getName());
		return node;
	}
}
