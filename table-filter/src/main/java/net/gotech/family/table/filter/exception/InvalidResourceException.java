package net.gotech.family.table.filter.exception;

public class InvalidResourceException extends Exception {

	public InvalidResourceException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
