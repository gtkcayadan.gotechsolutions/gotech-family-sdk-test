package net.gotech.family.table.filter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown=true)
public class TableParam extends StringUtils{

	// parameters required from vue-good-table components
	private Map<String, String> columnFilters;
	// user can pass dynamic parameter using this object
	private Map<String, String> userParams;
	// sorting
	private Map<String, String> sort;
	// current page
	private int page;
	// number of data to be display
	private int perPage;
	// Holds values of the parameters to be used for prepared statement
	private List<Object> values = new ArrayList<Object>();
	// Holds values of the condition queries to be used for prepared statement
	private String condition = "";
	
	private Map<String, Value> columnKeys;
	
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	
	public Map<String, String> getColumnFilters() {
		return columnFilters;
	}
	public void setColumnFilters(Map<String, String> columnFilters) {
		this.columnFilters = columnFilters;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPerPage() {
		return perPage;
	}
	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}
	public Map<String, String> getSort() {
		return sort;
	}
	public void setSort(Map<String, String> sort) {
		this.sort = sort;
	}
	
	public String orderField(){
		if(sort.get("field").toString().isEmpty()){
			return "id";
		}
		return sort.get("field").toString();
	}
	
	public String orderType(){
		if(sort.get("type").toString().isEmpty()){
			return "DESC";
		}
		return sort.get("field").toString();
	}
	
	public String orderBy(){
		if(sort.get("field") == null || sort.get("type") == null || sort.get("field").toString().trim().isEmpty() || sort.get("type").toString().trim().isEmpty()){
			return "";
		}
		System.out.println(columnKeys);
		return "order by "+columnKeys.get(sort.get("field").toString()).getColumn() + " " + sort.get("type").toString();
	}
	
	public String generateFilters(){
		if(getColumnFilters() != null) {
			for (Map.Entry<String, String> entry : getColumnFilters().entrySet()) {
				if(entry.getValue() != null && !entry.getValue().isEmpty()){
					String key = entry.getKey();
					Value vk = columnKeys.get(key);
					String value = entry.getValue();
					
					if(key.equalsIgnoreCase("status") && !value.isEmpty() && vk.getType() == DataType.BOOLEAN){
						condition += ( " and " + vk.getColumn() + " = ? ");
						values.add(Boolean.parseBoolean(value));
					}else if(!value.isEmpty()){
						switch (vk.getType()) {
						case STRING:
							condition += (" and lower(" + vk.getColumn() + ") like ? ");
							values.add("%"+value.toLowerCase()+"%");
							break;
						case LONG:
							condition += (" and " + vk.getColumn() + ":text LIKE ? ");
							values.add("%"+value+"%");
							break;
						case TIMESTAMP:
							condition += (" and " + vk.getColumn() + " = ? ");
							values.add(value);
							break;
						case DATE:
							condition += (" and " + vk.getColumn() + " = ? ");
							values.add(value);
							break;
						default:
							break;
						}
					}
				}
			}
		}
		return condition;
	}
	
	public String generateFiltersWithWhere(){
		String condition = generateFilters();
		if(!condition.isEmpty()){
			return " where " + condition.substring(4, condition.length());
		}
		return condition;
	}
	
	public void addCondition(String string){
		if(condition.isEmpty()){
			condition = " where " + string+ " ";
		}else
			condition += " and "+string+ " ";
	}
	
	public String sortAndPaginate(){
		if(getPerPage()==0) {
			return orderBy();
		}
		addParam(getPerPage()*getPage());
		addParam(getPage()-1);
		return orderBy() +" LIMIT ? OFFSET ?";
	}	
	
	public long limit(){
		return getPerPage();
	}
	
	public long offset(){
		return (getPerPage()*(getPage()-1));  
	}
	
	public Object[] getValues(){
		return values.toArray();
	}
	
	public Map<String, String> getUserParams() {
		return userParams;
	}
	public void setUserParams(Map<String, String> userParams) {
		this.userParams = userParams;
	}
	
//	public Map<String, String> getColumnFiltersTypes() {
//		return columnFiltersTypes;
//	}
//	public void setColumnFiltersTypes(Map<String, String> columnFiltersTypes) {
//		this.columnFiltersTypes = columnFiltersTypes;
//	}
	public void addParam(Object object){
		values.add(object);
	}
	
	
	public Map<String, Value> getColumnKeys() {
		return columnKeys;
	}
	public void setColumnKeys(Map<String, Value> columnKeys) {
		this.columnKeys = columnKeys;
	}
	@Override
	public String toString() {
		return "TableParam [columnFilters=" + columnFilters + ", userParams=" + userParams + ", sort=" + sort
				+ ", page=" + page + ", perPage=" + perPage + ", values=" + values + ", condition=" + condition
				+ ", columnKeys=" + columnKeys + ", format=" + format + "]";
	}
	
}
