package net.gotech.family.table.filter;

public enum DataType {

	STRING, NUMBER, DATE, TIMESTAMP, INTEGER, LONG, FLOAT, DOUBLE, BOOLEAN, JSON, JSON_ARRAY
	
}
