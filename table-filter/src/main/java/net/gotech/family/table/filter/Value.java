package net.gotech.family.table.filter;

public class Value {

	private String column;
	private DataType type;
	
	public Value(String column, DataType type) {
		this.column=column;
		this.type = type;
	}
	
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public DataType getType() {
		return type;
	}
	public void setType(DataType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Value [column=" + column + ", type=" + type + "]";
	}
	
}
