package net.gotech.family.table.filter.exception;

public class ResourceProcessingException extends Exception {

	public ResourceProcessingException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
