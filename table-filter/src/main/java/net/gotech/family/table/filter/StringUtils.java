package net.gotech.family.table.filter;

import org.apache.commons.lang.RandomStringUtils;

public class StringUtils {

	public boolean isNumeric(String string){
//		return string.chars().allMatch( Character.is );
		return true;
	}
	
	public Long toLong(String string){
		return Long.parseLong(string);
	}
	
	public static String append(String where, String string){
		if(where.trim().isEmpty()){
			return " where "+string;
		}
		return where = appendWithAnd(where, string);
	}
	
	public static String appendWithAnd(String where, String string){
		return where += " and "+string;
	}
	
	public static String generateCode(){
		int length = 5;
	    boolean useLetters = true;
	    boolean useNumbers = true;
	    String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
	    return generatedString.toUpperCase();
	}
	
	public static void main(String[] args) {
		System.err.println(generateCode());
	}
}
