package net.gotech.family.smtp.server;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.smtp.server.model.Content;

@RestController
@RequestMapping(value = "/SendEmail")
public class SmtpServerRestController {

	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private ObjectMapper objectMapper;

	@RequestMapping(value= "", method=RequestMethod.POST, produces="application/json")
	public JsonNode sendEmail(@RequestBody Content content) throws MessagingException, IOException {
		ObjectNode node = objectMapper.createObjectNode();

		ExecutorService executor = Executors.newFixedThreadPool(2);

		executor.submit(() -> {
			try {
				sendEmail1(content);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		
		executor.shutdown();

		node.put("status", "OK");
		node.put("message", "Successfully sent!");

		return node;
	}

	String generateContentId(String prefix) {
		return String.format("%s-%s", prefix, UUID.randomUUID());
	}

	public Resource getImage() throws IOException {
		URL url = new URL("http://upload.wikimedia.org/wikipedia/commons/9/9c/Image-Porkeri_001.jpg");
		InputStream in = new BufferedInputStream(url.openStream());


		Resource res = new InputStreamResource(url.openStream());

		System.out.println(in);

		return res;
	}

	public void saveImage(String imageUrl, String destinationFile) throws IOException {
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	}

	@Async
	private void sendEmail1(Content content) throws Exception {
		MimeMessage message = javaMailSender.createMimeMessage();


		//		String imageUrl = "http://192.168.1.35:8080/images/2019-09-12/-6cbe3c97-63fe-4e19-bf9f-3bee334f9dfd.jpg";
		//		String destinationFile = "D://image.jpg";

		//		saveImage(imageUrl, destinationFile);

		// Enable the multipart flag!
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		helper.setTo(content.getTo());
		if(content.getCc() != null) {
			helper.setCc(content.getCc());
		}
		if(content.getBcc() != null) {
			helper.setBcc(content.getBcc());
		}


		//        String contentId = ContentIdGenerator.getContentId();

		//        helper.setText(
		//        		"<body>\r\n" + 
		//        		"<div  style=\"background-image: url(cid:" + contentId + ") center center; background-size: cover; position: relative; color: #333333; width: 735px; height: 830px; margin: auto; border-radius: 10px; padding: 140px 150px; display: block; font-family: 'Rubik', sans-serif; box-sizing: border-box;\">\r\n" + 
		//        		 
		//        		"<h1 style=\"color: #948C4F; font-weight: 300; font-size: 36px; line-height: 32px;\">Email Verification</h1>\r\n" + 
		//        		"<br>\r\n" + 
		//        		"<span style=\"font-weight: normal; font-size: 18px; line-height: 36px; align-items: center;\">Good Day,<br>Thank you for using Hlacyon Health Tracker. Kindly verify your email by submitting the code below in Halcyon PEME App.</span>\r\n" + 
		//        		"<p style=\"margin: 70px 0px;font-weight: bold; font-size: 36px; line-height: 32px; display: flex; align-items: center; color: #009BD1;\">123456</p>\r\n" + 
		//        		"<span style=\"font-size: 18px; line-height: 32px;\">Regards,<br><span style=\"font-weight: bold;\">The Halcyon Team</span></span>\r\n" + 
		//        		"</div>\r\n <img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAPEBAPDxAPDxAPDw4PDw8PDw8PDw0PFREXFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLSstNy0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLTgtLS0yN//AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAwECBAUGB//EAD0QAAICAQIEAwQFCgYDAAAAAAABAhEDITEEEkFRBWGxE4GRoSJDcZLRBgcyQlJTgsHh8BQVI2KDk2Nz8f/EABgBAAMBAQAAAAAAAAAAAAAAAAECAwAE/8QAIxEAAwEAAgICAgMBAAAAAAAAAAECEQMhEjEEQRNRIqHRsf/aAAwDAQACEQMRAD8A+LgAHcRAAAxgAAMYKJAAowABKGAQTRNAHAEEgkWSDhiEiyRZRLqOnn9mlfaMpF0Xyk8o1QLrGMpF8jNykOI9wI5DeIdFRgRM0SjSENAawKeiyKLNEUJgxWiSSBQkAAAMQBJAAgAAAwAAGMOxbL3+oBj2QChEgADAAAAxgJAAmAkCaGSARQFqBobAEFkQol1AZJgbDlLRiMx42aceKykwTdGeOMdHF/PTt5/32NWPhWa8XA30LTxkq5EjnRxGmPDOn/LY6v8AluiaT2qV1+l5eR0cPhMvZ83K+VtpOtG1uvmiq4s9kK50eSlgJhw/Vnof8sbewvieCrRID4grmT6POZcZmnE6/EYOiMWTFRGoOibMLiLZrlgb6Mh8M1uRcMqqRkonlHNJFJSF8Q6U5SrLFWK0MQBJAgSAJIFCAABjDsWy9/qAYtl7/UBQiQABgAAE0YwAiUiyiMkDStDEiY47/tDYYH5fFFZTEbFpFuU0QwP/AG/eRox8Hf60PvRKqdEdJGXHgs04uDs24ODX7UPvx/E6WHg0954/vw/EvEIhfLhzIeHtvTU0f5fJVoeh4PgVova4l/yQX8zrcL4dDrkxf9kPxLKEctc7R5zw3gX2O7wvhF9KZ6LheEhabng2S+jLHFV7up2uH4bh+uTEv+SAtcnictXVvo87wXgqpprddupoj4M4xarY9Vw8sEWqy4lXecGbJ5uFerz4be/00Qr5VJ5jAuG67PAy8IaTbRxOO8NbtJP4H07iv8K/r8L/AI4nL4lcP+9w/a8kPxKR8jy9o3jcM+Z5PAZN9K95j4rwiOPf6T8j6Fk/w6eubDX/ALIHK8QhwzTrPh1/8sSupjzzXvo+d8Smv0Y0c3Ljkz2nE8Jh/fYf+yJnfhuJrTLh8/8AUiJXGn9nZPPn0eKngYqUGj2GfwnH0y4/vx/E5XF+HqP6+NrynH8SNcP6OiOdM4TRWjZPB5x+8hXsdd18VRzuWdCoRRDQ1xKSA0FMWQy1EEmMQAAKEdi2Xv8AUAx7AAIkCSaCAhItRKRZRHS0VshIlIvyAkP4i6XhAsiyRZROlSTbKMmMn/XsXaIUQUuzJgsjHwnPl5k9L5d1dtXtvWm4qi3KFIDGR4qfdjpcZNbTb0i71WrSte52vcZuQbBaFJT9CPDRj47N+2172WfimZfry+LKYorqVz49fhsXcZOpk+t9DV4zn/eS+LJj4vnentJfeoyOAyGPSya8m/YWp/Q2Xiub9uXxYt+JZX+s/iLnAryAflvsKU/ou+NyP9ZlcuaabUnqm09dmi/sUuz22KZl5dAOXmhTWiZZpdx+LM63EuBMHQIePsLWo0Sk+W7Wrarqttfn8mZssm0Pjy07vbSq38/LczZH0LXSwWUZmiHHsNcSeU4lOl9EOJRxNLXQXKIKgKYhoq0NaKtEGh0xbRWhjRViNDF4bATBaAAItIskBZDCgkNgisYllErKwRl2iEi7dpKqr5gkUfYpMGPxuOt3tpVb+fkKSLqJSaaEZD1LrGWhEeolJny7YrrDOoF4wHTgMwRVO99K12HUd4I66ErGMx4xygWUaLTGMR0K5PLUHAfpZOVa9PdsM0hdMcoAomjkDkJYN5CsmPRap6af7dXp5f1F8hqUO+3kVcDNB8jPQRjb2u9Ou45wLcmmhlOm8jHPG06aqt12ZRxNc8bKKF+/v0EcjKjK4lXE0chEoCeDG8jK4hHsOkhde8TMY+kOAqSo0YsnK02lJJ3yyun5OhM1YKazoyE5Hbb017JJfAW0NaKNEGVQtoq0MaKtE2hkTBaAWhsAgwtFkQShkKOxl0xSLRLzQjQ2TBIiKGKDH7Yj6JjEa0ummxEZPRPZfIvOVtukrbdJUl9g6QrIijTjiJiaoovxIlbDLgaSdp8yezTa1qmuhWCHqNp20qTet6+WhQpU4+hEy8aH8lxtJ/RX0nq0ren2CFEbFjqhGiigXUfIZFWXcKNgNKRxJ/z+0l4RkNP71JciiSwRtiXg0vT4q/gKcDQ/t93YihMG0zezGVW2n2aD4NW3K9nXKlv09xWSGlGbM0oiZKr0u18DW0tfkZsu4vIuhpYuELTfYrPHt56otysrKRPrOygmcRPLeiHzdl1Bwppq6jJOLTq1a22ZJyqY6eIzZuHcW06taOmn81oyuWSpRV1o3dfpda8h0229d2Zs0abXo7QlypXQ8vSssa5U7Vttcutqq17f/DO0NbFyOaiqFsqy7Kskxy8NgCGwExikIWm7SpXr11rQEUTGONU9Ndd13oogF5Tvt02SRMRaLpjJis0YoaX0VL+/gasWWotUndaveNPoYYyHLIdPHaRKp0vIlC1Idj3GX8n0K+kMjFmjC6u02kunTt86I4bltczaja5nFJyS60m1bJkdMxi1EW96DmsfFaUZISNMJXt16Bh77BSL1Q3Ek7tpUrW+r7GeUy3PV9PLsFtaLhogxuWTbuTbctW27bfdsyY5jZZPiOswRrsuShMp6vp5bURzg02Gx4nHSSadbNU1f2ipSa0vTf8Av4lP8QKlksams6Apf2XcivPRSwm2rjeml09H29RNHwJTsjlq9rtrRpr4ipSBTrfrqhVXfY3j+jVFqta95hnC5Ovnp6j4ZEtd308hE5FOVqpQIWNlMSV021vqknrWnzEynQzJj05tKut1d1e29eZnbOOm0XS0tLIIk7HTguVO1q2q1tVWr8nfyYmO5Om30x0QkHEKNvlTUb05mm682i8o77aFJybq3tovJGc9YMmZ2iI1rfbTS7d9e3UvIpOqVXet9vL+Zz0sKImGwBBqiSeDGdFrF2WTF0JdMZEVAcmkVhaIy0R2SMdeWVpPS1TfnRn5wUimpC4OTH4ZdDKpF4yHisei1JvRPPQvJxnMk3rLZvRLlUUkqS30EPJZ0vlleiPg/sfzDFkMqkPhJNVWt7307UJL0LQ1TLc4lLzLQk5Pu935lFu4K0OUyVMroL60O5aF9j+cOciLVVS+3qhc2FpoCHc5M2k6tPXdXT81Zl5yHMTyG8Ta5g9r6GPmb76BLiJNKNuldJvRXvXYf8v7QvgXyS1FymL5iyRLun0UzC+OfQicxUxUpgdtLGFTpecxTkQ5AokW3RRLAbZXmSem38gySdVem6XT7fkhLZOnjGSNzlB8qSa0+k7vmdvVaaaUq128xOdJbGZToh5Cj55c412BQ0y02IbJlIq2cl1pZIbB6AVg9AJjCUy+KDk6WujfuSt/JCyUzIzL3RbmFWSmFMGDUyyYpMsmOmDBqZZSFqu5eMV3+RRMVjI2ybNXD4oNa5Ev4WXnw0G9Mifnysv4fx0l5rcMvRO1u1WtrbX5/IZhyUbMfh0X9bH4SNmHwWL+uh8JDSqT0Srn7OY5hw+RJ/SbSp6pW7rTqup6LF+TUX9fj+E/wN/D/kbCVf68PdGf4FX5amRfNxr7PJPIiuOTbZ9AwfkHjf18PuyOnw35usVprNH7sqC+XGvJifn4/pnzG32FzbPt2H83fCyX6ST66uvdoL4j82OBr6OSKXVvm/ASvlcL68v6Gmm+8/tf6fELIs+w5PzbcOvrV7lL8DLm/IDho/XL4S/AVVL9P/v+GfyIXs+XQnoLrm5npor1aV6pad3rt9p9Gz/kXwy+uX3Wc7P+S2BbZo/dkVe0sAvkcenheY0y5eVNNt68yqku1O9fkeh4jwDDHbNF/wAMjDl8Nxx+tX3ZAlOSn5Yr0cXJIzuR1c3CY/3i+6zLmww/eXWn6LJcmtlZaMTkXWQmcI/tfITJLv8AIkqclMTJyzFNg6KNkqrWOkS2VbIbKtk2x0iWyLIsgTQj1K9drvRbLUCkHoANDgoAAxiQsgDGL2TZQmxkDC6ZZSF2TYyYMHRyDYZjLZKHVMVydHHxTRsw8c+5yMce5qxzjHzLzbI1CO/wvHSe1nc4Pi5LVyr3njY+IV+joMhxsnuzonlRy3wafRuG8YjH9a2vmdXg/HXJ/wBT5lw3FM7PCcdS3H/jRyX8fD6Zh8aUVua8fjqVXTvvsfLX4u29zXxHi65FTlz27drl5a0rz3Efx+N+yf47Xo9f4l4xTd6de2hwOL8al0laPPcV4lKa5lbpLmfRa0rOXl499ymTPSHj47fs9Dm/KCStVF2q1SbX2djlcR4vZyMvFKXkzBmyMSuTPR1x8dHVzeIp3662jBm4q+pz55RMshz1ys6p4UjXkzMzyyCXIq5EXZVQMlMo5FeYhsRsdIlsiyLIYjYcJshsgGKxiAAAGH4tl7/UAxbIABEgABAAABjASVJCYkmiACAuieYoCG0GF/aMlSFk2NoMHxkOhkMiY2Mh1QrR0sOY1S4ulRyscq1IeWyyvERfHrOnDitTRk4q0ldebuvkcaOQvPKMuToV8fZrfEsTPL1tb1vr8DK8hWT0vvYrsdQMnlFPMLbKtknRRSWlKxbBsrYjY6QWQSQKECAAUIABAAgAAKYAAgxh+LZe/wBQDFsvf6gKEX7N9vQPZvt6AATB7N9vQPZvt6ABjByPt6B7N9vQAMYOR9vQn2b7egAFMAcj7egezfb0JAYwcj7egcrAA6DCVFl4RYAMmBovJshJgA2i4TqS2wAOmwrTIaYABs2FWmVcWAC6NhDiyfZvt6EABsIcj7egcj7egALpsI5H29A5H29AAGhD2b7ehHI+3oAA0Iezfb0D2b7egAAwezfb0D2b7egAYw7HjdL+gAADH//Z\" />", true);

		helper.setText(content.getMessage(), true);
		helper.setSubject(content.getSubject());

		//        ClassPathResource file = new FileSystemResource(new File(destinationFile));
		//        helper.addInline("1.1569224449668@DESKTOP-6TVHJ0S", new FileSystemResource(new File(destinationFile)));



		//        ClassPathResource background = new FileSystemResource(destinationFile);
		//        helper.addInline(contentId, new FileSystemResource(new File(destinationFile)));
		javaMailSender.send(message);
	}
}
