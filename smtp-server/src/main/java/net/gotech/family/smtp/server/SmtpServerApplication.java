package net.gotech.family.smtp.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class SmtpServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmtpServerApplication.class, args);
	}
}
