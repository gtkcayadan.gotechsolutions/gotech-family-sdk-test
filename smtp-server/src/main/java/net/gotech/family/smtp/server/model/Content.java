package net.gotech.family.smtp.server.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Content {
	
	private String[] to;
	private String message;
	private String subject;
	private String[] cc;
	private String[] bcc;
	public String[] getTo() {
		return to;
	}
	public void setTo(String[] to) {
		this.to = to;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	public String[] getBcc() {
		return bcc;
	}
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
	
	public String getToAsString(){
		return String.join(",", getTo());
	}
	
	public String getCCAsString(){
			return getCc() != null ? String.join(",", getCc()) : "";
		
	}
	
	public String getBCCAsString(){
		return getBcc() != null ? String.join(",", getBcc()) : "";
	}
	
	@Override
	public String toString() {
		return "Content [to=" + Arrays.toString(to) + ", message=" + message + ", subject=" + subject + ", cc="
				+ Arrays.toString(cc) + ", bcc=" + Arrays.toString(bcc) + "]";
	}
	
}
