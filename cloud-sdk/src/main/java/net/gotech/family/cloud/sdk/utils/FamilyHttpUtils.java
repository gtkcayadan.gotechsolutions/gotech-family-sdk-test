package net.gotech.family.cloud.sdk.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.cloud.sdk.entity.User;
import net.gotech.family.cloud.sdk.exception.AccessDeniedException;

@Service
public class FamilyHttpUtils {

	@Autowired
	private FamilyTemplate familyTemplate;
	
	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	public ObjectMapper objectMapper;
	
	private static final String AUTH = "Authorization";
	private static final String ORGANIZATION = "Organization";
	
	public ResponseEntity<JsonNode> httpGetRequest(String url,Map<String, String> map) throws AccessDeniedException, IOException{
		ObjectNode node = objectMapper.createObjectNode();
		if(map != null) {
			url = url+"?"+urlEncodeUTF8(map);
		}
		System.err.println("url : "+url);
		return httpRequest(url, HttpMethod.GET, node, null);
	}
	
	public ResponseEntity<JsonNode> httpPostRequest(String url,Object map) throws AccessDeniedException, IOException{
		return httpRequest(url, HttpMethod.POST, map, null);
	}
	
	public ResponseEntity<JsonNode> httpPutRequest(String url,Object map) throws AccessDeniedException, IOException{
		return httpRequest(url, HttpMethod.PUT, map, null);
	}
	
	public ResponseEntity<JsonNode> httpDelRequest(String url,Object map) throws AccessDeniedException, IOException{
		return httpRequest(url, HttpMethod.DELETE, map, null);
	}
	
	public ResponseEntity<JsonNode> httpPatchRequest(String url,Object map) throws AccessDeniedException, IOException{
		return httpRequest(url, HttpMethod.PATCH, map, null);
	}
	
	public ResponseEntity<JsonNode> httpRequest(String url, HttpMethod method, Object request_body, String auth) throws IOException{	
		RestTemplate restTemplate = familyTemplate.getRestTemplate();
		restTemplate.setErrorHandler(new FamilyErrorHandler());
		
		
	    HttpHeaders headers = new HttpHeaders();
	    
	    setAuthHeader(headers, auth);
	    
	    headers.set("Content-Type", "application/json");
	    HttpEntity<Object> entity = new HttpEntity<>(request_body, headers);
	    
	    HttpClient httpClient = HttpClients.createDefault();
	    restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient)); 
	    
	    
	    
	    ResponseEntity<JsonNode> response = restTemplate.exchange(url, method, entity, JsonNode.class);
	    
		if(response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			System.out.println(url);
			System.out.println(auth);
			System.out.println("UNAUTHORIZED :: "+response);
			throw new AccessDeniedException();
		}
		return response;
	}
	
	private String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
	private String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                urlEncodeUTF8(entry.getKey().toString()),
                urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();       
    }
	
	
	public ResponseEntity<String> httpRequestXml(String url, HttpMethod method, Object request_body) throws IOException{	
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new FamilyErrorHandler());
	    HttpHeaders headers = new HttpHeaders();
	    
	    setAuthHeader(headers, null);
		
	    headers.set("Content-Type", "application/xml");
	    HttpEntity<Object> entity = new HttpEntity<>(request_body, headers);
	    
	    HttpClient httpClient = HttpClients.createDefault();
	    restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient)); 

	    ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class);
	    
		if(response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			throw new AccessDeniedException();
		}
		return response;
	}
	
	private HttpHeaders setAuthHeader(HttpHeaders headers, String authString) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		try {
			User user = (User) auth.getPrincipal();
			headers.add(ORGANIZATION, user.getOrganization().getId()+"");
		} catch (Exception e) {
			
		}
		
		
		if(authString != null) {
			headers.add(AUTH, authString);
		}else {
			auth = SecurityContextHolder.getContext().getAuthentication();
			
			if(!(auth instanceof AnonymousAuthenticationToken)) {
				try {
					headers.add(AUTH, auth.getDetails().toString());
				} catch (Exception e) {
					if(httpServletRequest != null && httpServletRequest.getHeader("Authorization") != null){				
						headers.add(AUTH, httpServletRequest.getHeader("Authorization"));
					}
				}
			}else if(httpServletRequest != null && httpServletRequest.getHeader("Authorization") != null){				
				headers.add(AUTH, httpServletRequest.getHeader("Authorization"));
			}
		}
		return headers;
	}
	
	public ResponseEntity<JsonNode> httpGetRequest(String url,Map<String, String> map, String auth) throws AccessDeniedException, IOException{
		ObjectNode node = objectMapper.createObjectNode();
		if(map != null) {
			url = url+"?"+urlEncodeUTF8(map);
		}
		System.err.println(url);
		return httpRequest(url, HttpMethod.GET, node, auth);
	}
}
