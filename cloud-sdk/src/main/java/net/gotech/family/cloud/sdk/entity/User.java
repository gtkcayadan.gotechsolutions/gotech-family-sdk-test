package net.gotech.family.cloud.sdk.entity;

public class User {
	
	private long id;
	private Organization organization;

	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public User(long id, Organization organization) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.organization = organization;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", organization=" + organization + "]";
	}
	
}
