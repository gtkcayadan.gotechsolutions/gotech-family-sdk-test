package net.gotech.family.cloud.sdk.entity;

public class Organization {

	private long id;
	private String name;
	
	public Organization() {
	}
	
	public Organization(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Organization [id=" + id + ", name=" + name + "]";
	}
	
	
	
}
