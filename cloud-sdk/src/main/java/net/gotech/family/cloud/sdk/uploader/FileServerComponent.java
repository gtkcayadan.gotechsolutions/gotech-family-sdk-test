package net.gotech.family.cloud.sdk.uploader;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.cloud.sdk.exception.InvalidJwtPathException;
import net.gotech.family.cloud.sdk.utils.FamilyTemplate;

@Component
public class FileServerComponent {

	@Value("${server.cloud.path.fileserver:#{null}}")
	private String url;

	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private FamilyTemplate familyTemplate;

	public JsonNode sendFileToServer(MultipartFile multipartFile, String path) throws IOException, InvalidJwtPathException {

		if(url == null) {
			throw new InvalidJwtPathException("File Server Path is required to use this function. Add server.cloud.path.fileserver = <FILE_SERVER_PATH> in your application.properties.");
		}

		final String filename = multipartFile.getOriginalFilename();
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		ByteArrayResource contentsAsResource = new ByteArrayResource(multipartFile.getBytes()) {
			@Override
			public String getFilename() {
				return filename;
			}
		};
		map.add("file", contentsAsResource);
		map.add("type", path);
		
		System.out.println(contentsAsResource.getFilename().split("\\.")[1]);
		map.add("file", contentsAsResource);
		map.add("type", path);
		try {
			map.add("fileType", contentsAsResource.getFilename().split("\\.")[1]);
		} catch (Exception e) {
		}
		String result = familyTemplate.getRestTemplate().postForObject(url+"/file-upload-2", map, String.class);
		ObjectNode node = objectMapper.createObjectNode();
		node.put("status", "OK");
		JsonNode response = objectMapper.readTree(result);
		String name = response.get("domain").asText() + "/" + response.get("file").asText();

		node.put("data",name);

		return node;
	}

	public JsonNode sendFileToServerV2(MultipartFile multipartFile, String path) throws IOException, InvalidJwtPathException {
		if(url == null) {
			throw new InvalidJwtPathException("File Server Path is required to use this function. Add server.file.server.path = <FILE_SERVER_PATH> in your application.properties.");
		}

		final String filename = multipartFile.getOriginalFilename();
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		ByteArrayResource contentsAsResource = new ByteArrayResource(multipartFile.getBytes()) {
			@Override
			public String getFilename() {
				return filename;
			}
		};
		System.out.println(contentsAsResource.getFilename().split("\\.")[1]);
		map.add("file", contentsAsResource);
		map.add("type", path);
		try {
			map.add("fileType", contentsAsResource.getFilename().split("\\.")[1]);
		} catch (Exception e) {
		}
		String result = familyTemplate.getRestTemplate().postForObject(url+"/file-upload-2", map, String.class);
		ObjectNode node = objectMapper.createObjectNode();
		node.put("status", "OK");
		JsonNode response = objectMapper.readTree(result);
		String name = response.get("domain").asText() + "/" + response.get("file").asText();

		ObjectNode node2 = objectMapper.createObjectNode();
		node2.put("file", name);
		node.set("files", node2);
		node.put("url", response.get("domain").asText());

		return node;
	}
}
