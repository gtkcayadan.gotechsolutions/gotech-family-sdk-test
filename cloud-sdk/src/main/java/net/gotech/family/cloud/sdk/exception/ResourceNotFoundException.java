package net.gotech.family.cloud.sdk.exception;

import java.io.IOException;

public class ResourceNotFoundException  extends IOException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ResourceNotFoundException(String message) {
		super(message);
	}
	
	

}