package net.gotech.family.cloud.sdk.rp;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;

import net.gotech.family.cloud.sdk.exception.AccessDeniedException;
import net.gotech.family.cloud.sdk.exception.InvalidJwtPathException;
import net.gotech.family.cloud.sdk.utils.FamilyHttpUtils;

@Component
public class InfoRequest {

	@Value("${server.cloud.path.management:#{null}}")
	private String url;
	
	@Autowired
	private FamilyHttpUtils familyHttpUtils;
	
	public ResponseEntity<JsonNode> getUser(long id) throws AccessDeniedException, IOException, InvalidJwtPathException {
		if(url == null) {
			throw new InvalidJwtPathException("Management Path is required to use this function. Add server.cloud.path.management = <MANAGEMENT_PATH> in your application.properties.");
		}
		return familyHttpUtils.httpGetRequest(url+"/User/"+id, null);
	}
	
	public ResponseEntity<JsonNode> getPermissions(JsonNode node) throws AccessDeniedException, IOException, InvalidJwtPathException {
		if(url == null) {
			throw new InvalidJwtPathException("Management Path is required to use this function. Add server.cloud.path.management = <MANAGEMENT_PATH> in your application.properties.");
		}
		return familyHttpUtils.httpPostRequest(url+"/RolePermission", node);
	}

	public String getUrl() {
		return url;
	}
	
}
