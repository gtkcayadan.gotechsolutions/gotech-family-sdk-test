package net.gotech.family.cloud.sdk.utils;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

public class FamilyErrorHandler implements ResponseErrorHandler {
	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		System.err.println("ERROR");
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return false;
		
	}
}