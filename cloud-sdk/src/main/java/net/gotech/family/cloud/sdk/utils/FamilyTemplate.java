package net.gotech.family.cloud.sdk.utils;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FamilyTemplate {

	private RestTemplate restTemplate;
	
	public FamilyTemplate() {
		
		System.out.println("FamilyTemplate initialize...");
	}
	
	public RestTemplate getRestTemplate(){
		if(restTemplate == null){
			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			factory.setConnectionRequestTimeout(10000);
			factory.setConnectTimeout(10000);
			factory.setReadTimeout(10000);
			restTemplate = new RestTemplate(factory);
			restTemplate.setErrorHandler(new FamilyResponseErrorHandler());
		}
		
		return restTemplate;
	}
}
