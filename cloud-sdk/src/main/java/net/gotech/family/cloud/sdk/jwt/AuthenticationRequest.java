package net.gotech.family.cloud.sdk.jwt;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;

import net.gotech.family.cloud.sdk.exception.AccessDeniedException;
import net.gotech.family.cloud.sdk.exception.InvalidJwtPathException;
import net.gotech.family.cloud.sdk.utils.FamilyHttpUtils;

@Component
public class AuthenticationRequest {
	
	public AuthenticationRequest() {
		System.out.println("AuthenticationRequest...");
	}

	@Value("${server.cloud.path.jwt:#{null}}")
	private String url;
	
	@Autowired
	private FamilyHttpUtils familyHttpUtils;
	
	public ResponseEntity<JsonNode> getToken(JsonNode node) throws AccessDeniedException, IOException, InvalidJwtPathException {
		if(url == null) {
			throw new InvalidJwtPathException("JWT Path is required to use this function. Add server.jwt.path = <JWT_PATH> in your application.properties.");
		}
		return familyHttpUtils.httpPostRequest(url+"/Create", node.toString());
	}
	
	public ResponseEntity<JsonNode> isValidToken() throws AccessDeniedException, IOException, InvalidJwtPathException {
		if(url == null) {
			throw new InvalidJwtPathException("JWT Path is required to use this function. Add server.jwt.path = <JWT_PATH> in your application.properties.");
		}
		return familyHttpUtils.httpGetRequest(url+"/Validation", null);
	}
	
	public ResponseEntity<JsonNode> isValidToken(String token) throws AccessDeniedException, IOException, InvalidJwtPathException{
		if(url == null) {
			throw new InvalidJwtPathException("JWT Path is required to use this function. Add server.jwt.path = <JWT_PATH> in your application.properties.");
		}
		return familyHttpUtils.httpGetRequest(url+"/Validation", null, token);
	}
	
}
