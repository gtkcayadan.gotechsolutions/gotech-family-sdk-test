package net.gotech.family.notification.enums;

public enum Command {

	SKIP("MISSED"),
	DONE("DONE"),
	RESCHEDULE("ACTIVE"),
	ACTIVE("ACTIVE"),
	UPDATE("Update"),
	NOTIFIED("Notified")
	;
	
	
	private String label;

	private Command(String label) {
		this.label=label;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
