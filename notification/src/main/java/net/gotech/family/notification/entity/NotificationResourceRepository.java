package net.gotech.family.notification.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationResourceRepository extends JpaRepository<NotificationResource, Long>{

	List<NotificationResource> findByUuid(String uuid);
}
