package net.gotech.family.notification.filter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import net.gotech.family.table.filter.DataType;
import net.gotech.family.table.filter.Value;

public enum NotificationFilter {

	DATE("date", new Value("date", DataType.DATE)),
	CONTENT("content",new Value("content",DataType.JSON_ARRAY)),
	NAME("name",new Value("name",DataType.STRING)),
	TYPE("type",new Value("type",DataType.STRING)),
	CATEGORY("category",new Value("category",DataType.STRING)),
	UUID("uuid",new Value("uuid",DataType.STRING)),
	OWNER("owner",new Value("owner",DataType.STRING)),
	NO_OF_ITEMS("no_of_items",new Value("no_of_items",DataType.INTEGER)),
	
	;
	
	private String key;
	private Value value;
	
	NotificationFilter(String key, Value value){
		this.key=key;
		this.value=value;
	}
	
	public static Map<String, Value> getMap(){
		return Arrays.stream(NotificationFilter.values())
			      .collect(Collectors.toMap(NotificationFilter::getKey, NotificationFilter::getValue));
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}
}
