package net.gotech.family.notification.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class NotificationContent {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String sysField;
	private String sysValue;
	@OneToOne(fetch = FetchType.EAGER)
	private NotificationResource resource;
	
	
	public NotificationContent() {
		// TODO Auto-generated constructor stub
	}
	
	public NotificationContent(String sysField, String sysValue, NotificationResource resource) {
		this.sysField = sysField;
		this.sysValue = sysValue;
		this.resource = resource;
	}
	
	public NotificationResource getResource() {
		return resource;
	}
	public void setResource(NotificationResource resource) {
		this.resource = resource;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSysField() {
		return sysField;
	}
	public void setSysField(String sysField) {
		this.sysField = sysField;
	}
	public String getSysValue() {
		return sysValue;
	}
	public void setSysValue(String sysValue) {
		this.sysValue = sysValue;
	}
	@Override
	public String toString() {
		return "NotificationContent [id=" + id + ", sysField=" + sysField + ", sysValue=" + sysValue + ", resource="
				+ resource + "]";
	}
}
