package net.gotech.family.notification.entity;

import java.util.Map;

public class Notification {
	
	
	public Notification(NotificationResource resource, Map<String, String> content) {
		this.resource = resource;
		this.content = content;
	}
	
	private NotificationResource resource;
	private Map<String, String> content;
	
	public NotificationResource getResource() {
		return resource;
	}
	public void setResource(NotificationResource resource) {
		this.resource = resource;
	}
	public Map<String, String> getContent() {
		return content;
	}
	public void setContent(Map<String, String> content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "Notification [resource=" + resource + ", content=" + content + "]";
	}
}
