package net.gotech.family.notification.entity;

public class Content {
	
	private long resource_id;
	private String sys_field;
	private String sys_value;
	private long id;
	public long getResource_id() {
		return resource_id;
	}
	public void setResource_id(long resource_id) {
		this.resource_id = resource_id;
	}
	public String getSys_field() {
		return sys_field;
	}
	public void setSys_field(String sys_field) {
		this.sys_field = sys_field;
	}
	public String getSys_value() {
		return sys_value;
	}
	public void setSys_value(String sys_value) {
		this.sys_value = sys_value;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Content [resource_id=" + resource_id + ", sys_field=" + sys_field + ", sys_value=" + sys_value + ", id="
				+ id + "]";
	}
}
