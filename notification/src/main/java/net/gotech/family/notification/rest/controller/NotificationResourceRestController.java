package net.gotech.family.notification.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.notification.entity.Notification;
import net.gotech.family.notification.entity.NotificationContent;
import net.gotech.family.notification.entity.NotificationResource;
import net.gotech.family.notification.service.NotificationServices;


@EnableAutoConfiguration
@RestController
@RequestMapping(value= {"/NotificationResource"})
public class NotificationResourceRestController {
	
	@Autowired
	private NotificationServices notificationServices;
	
	
	@RequestMapping(value= "", method=RequestMethod.POST, produces="application/json")
	public List<NotificationContent> addTableFields(@RequestBody Notification notification){
		return notificationServices.create(notification);
	}

	@RequestMapping(value= "", method=RequestMethod.DELETE, produces="application/json")
	public JsonNode deleteTableFields(@RequestBody JsonNode request){
		
		return notificationServices.delete(request);
	}
	
	
	 @RequestMapping(value= "", method=RequestMethod.PUT, produces="application/json") 
	 public List<NotificationContent>addTableFields(@RequestBody List<NotificationContent> contents){ 
		 return notificationServices.update(contents); 
	 }
	 

	
	@RequestMapping(value= "", method=RequestMethod.GET, produces="application/json")
	public String getByDate(@RequestParam String date,@RequestParam String patient,@RequestParam boolean history,@RequestParam String type,@RequestParam Integer category){
		List<JSONObject> data = notificationServices.findbyOwnerAndDate(date, patient,history,type,category);
		return data.toString();
	}
	
	@RequestMapping(value="/History",method=RequestMethod.GET,produces = "application/json")
	public Object getAlarmHistory(String filters) {
		try {
			return notificationServices.getMedicineHistory(URLDecoder.decode(filters,"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
