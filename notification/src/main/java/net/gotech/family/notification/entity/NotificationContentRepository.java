package net.gotech.family.notification.entity;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NotificationContentRepository extends JpaRepository<NotificationContent, Long>{
	
	@Transactional
	void deleteByResource(NotificationResource resource);
	
	List<NotificationContent> findByResource(NotificationResource resource);
}
