package net.gotech.family.notification.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.valueextraction.Unwrapping.Skip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.gotech.family.notification.entity.Notification;
import net.gotech.family.notification.entity.NotificationContent;
import net.gotech.family.notification.entity.NotificationContentRepository;
import net.gotech.family.notification.entity.NotificationResource;
import net.gotech.family.notification.entity.NotificationResourceRepository;
import net.gotech.family.notification.enums.Command;
import net.gotech.family.notification.filter.NotificationFilter;
import net.gotech.family.table.dyanamic.service.BundleService;
import net.gotech.family.table.filter.TableParam;
import net.gotech.family.notification.entity.Content;

@Service
public class NotificationServices extends BundleService{

	@Autowired
	private NotificationResourceRepository notificationResourceRepository;
	@Autowired
	private NotificationContentRepository notificationContentRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	
	
	public List<NotificationContent> create(Notification notification) {
		NotificationResource resource = notificationResourceRepository.save(notification.getResource());
		List<NotificationContent> contents = new ArrayList<NotificationContent>();
		notification.getContent().entrySet().forEach(data -> {
			contents.add(new NotificationContent(data.getKey(), data.getValue(), resource));
		});
		return notificationContentRepository.saveAll(contents);
	}
	
	
	public ObjectNode delete(JsonNode request) {
		ObjectNode node = objectMapper.createObjectNode();
		node.put("status", "OK");
		
		NotificationResource notificationResource = null;
		try {
			notificationResource = objectMapper.readValue(request.get("resource").toString(), NotificationResource.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			node.put("status", "ERROR");
		}
		if(notificationResource != null) {
			if(request.get("delete_all").asBoolean()) {
				
				List<NotificationResource> resources = notificationResourceRepository.findByUuid(notificationResource.getUuid());
				if(resources != null) {
					resources.forEach(resource -> {
						try {
							delete(resource);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							node.put("status", "ERROR");
						}
					});
					
				}
			}else {
				try {
					delete(notificationResource);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					node.put("status", "ERROR");
				}
			}
		}
		
		return node;
		
	}
	
	private void delete(NotificationResource notificationResource) throws Exception{
			notificationContentRepository.deleteByResource(notificationResource);
			notificationResourceRepository.delete(notificationResource);
		
		
	}
	
	public List<NotificationContent> update(List<NotificationContent> contents){
		return notificationContentRepository.saveAll(contents);
	}
	
	public List<JSONObject> findbyOwnerAndDate(String date,String patientId,boolean isHistory,String type,Integer category){
		if(type != null) {
			if(type.toLowerCase().equals("medication")) {
				return findMedicationAlarmByOwnerAndDate(date, patientId, isHistory);
			}else if(type.toLowerCase().equals("workout")) {
				return findWorkoutAlarmByOwnerAndDate(date, patientId, isHistory,category);
			}else if(type.toLowerCase().equals("prescription")) {
				return getAllActivePrescriptionAlarm(patientId);
			}
		}
		
		return getAllActiveAlarmOfPatient(patientId, date);
	}
	
	public List<JSONObject> findMedicationAlarmByOwnerAndDate(String date,String patientId,boolean isHistory) {
		
		String status = isHistory ? "(t1.status = 'DONE' OR t1.status ='MISSED')" : "t1.status = 'ACTIVE'";
		
		final String SQL = "SELECT *,\n" + 
				"(SELECT json_agg((SELECT x FROM (SELECT nc.*, hm.name) AS x)) FROM notification_content as nc LEFT JOIN ht_medicine as hm ON nc.sys_value = CAST(hm.id AS varchar(10)) where nc.resource_id = t1.id) as content\n" + 
				" FROM (\n" + 
				"	SELECT r.*, \n" + 
				"		(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'date')) as date,\n" + 
				"	(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'status')) as status\n" + 
				"		FROM notification_resource r \n" + 
				"	where r.owner = ? and r.type='medication'" + 
				") as t1 where "+status+" and t1.date=?";
		return  getJdbcTemplate().query(SQL, new Object[] {patientId,date}, toJsonMapper(NotificationFilter.getMap()));
	}
	
	public List<JSONObject> findWorkoutAlarmByOwnerAndDate(String date,String patientId,boolean isHistory,Integer category) {
		
		final String SQL = generateSql(isHistory, category);
		return  getJdbcTemplate().query(SQL, new Object[] {patientId,date}, toJsonMapper(NotificationFilter.getMap()));
	}
	
	
	private String generateSql(boolean isHistory,Integer category) {
		String status = isHistory ? "(t1.status = 'DONE' OR t1.status ='MISSED')" : "t1.status = 'ACTIVE'";
		String includeCategory = category== null? "" : " and he.category_id="+category;
		
		return "SELECT t1.date, t1.status,t1.id,t1.uuid,t1.owner,hc.name as CATEGORY,\n" + 
				"(SELECT json_agg((SELECT x FROM (SELECT nc.*, hm.name) AS x)) FROM notification_content as nc LEFT JOIN ht_exercise as hm ON nc.sys_value = CAST(hm.id AS varchar(10)) where nc.resource_id = t1.id) as content\n" + 
				"	 FROM ( \n" + 
				"	SELECT r.*,  \n" + 
				"		(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'date')) as date, \n" + 
				"		(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'status')) as status,\n" + 
				"		(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'exercise')) as exercise\n" + 
				"		FROM notification_resource r  \n" + 
				"	where r.owner = ? and r.type='workout' \n" + 
				"	) as t1  \n" + 
				"	INNER JOIN ht_exercise he ON he.id = t1.exercise::int \n" + 
				"	INNER JOIN ht_exercise_category hc on hc.id=he.category_id\n" + 
				"	where "+status+" and t1.date=? "+includeCategory;
	}
	
	private List<JSONObject> getAllActiveAlarmOfPatient(String patient,String date) {
		String SQL = "SELECT t1.*,CASE  \n" + 
				"WHEN t1.type='workout' then he.name\n" + 
				"WHEN t1.type='medication' then hm.name\n" + 
				"END AS name, \n" + 
				"CASE WHEN t1.type='workout' then he.image END AS image,\n" + 
				"CASE WHEN t1.type='workout' then he.repetition END AS repetition,\n" + 
				"CASE WHEN t1.type='workout' then he.sets END AS sets,\n" + 
				"(SELECT json_agg(nc) FROM notification_content as nc where nc.resource_id = t1.id) as content\n" + 
				"FROM (SELECT r.*, \n" + 
				"(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'date')) as date, \n" + 
				"(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'status')) as status,\n" + 
				"(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'exercise')) as exercise,\n" + 
				"(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'medicine')) as medication\n" + 
				"FROM notification_resource r \n" + 
				"where r.owner = ?\n" + 
				") as t1  LEFT OUTER JOIN ht_exercise he on he.id=t1.exercise::int LEFT OUTER JOIN ht_medicine hm on hm.id=t1.medication::int where t1.status = 'ACTIVE' and t1.date=?";
		return  getJdbcTemplate().query(SQL, new Object[] {patient,date}, toJsonMapper(NotificationFilter.getMap()));
	}
	
	private List<JSONObject> getAllActivePrescriptionAlarm(String patient){
		String SQL = "SELECT t1.*,hp.no_of_items,hm.name, \n" + 
				"				(SELECT json_agg(nc) FROM notification_content as nc where nc.resource_id = t1.id) as content\n" + 
				"				 FROM ( \n" + 
				"					SELECT r.*, \n" + 
				"					(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'status')) as status,\n" + 
				"					(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'prescription')) as prescription\n" + 
				"						FROM notification_resource r \n" + 
				"					where r.owner = ? and r.type='prescription'\n" + 
				"				) as t1\n" + 
				"		INNER JOIN ht_prescription hp on t1.prescription::int = hp.id\n" + 
				"		INNER JOIN ht_medicine hm on hm.id=hp.medicine_id\n" + 
				"		\n" + 
				" where t1.status = 'ACTIVE'";
				return  getJdbcTemplate().query(SQL, new Object[] {patient}, toJsonMapper(NotificationFilter.getMap()));
	}
	
	private String generateHistoricalQuery(String params) {
		
		try {
			JSONObject filters = new JSONObject(params);
			System.err.println(filters);
			String addSelect="";
			String innerJoin="";
			String where="";
			String category="";
			if(filters.get("type").toString().equals("medication")) {
				addSelect="(select c.sys_value from notification_content c where c.resource_id=r.id and (c.sys_field='medicine'))as medicine";
				innerJoin="LEFT JOIN ht_medicine hm on hm.id=t1.medicine::int ";
				if(filters.has("medicine")) {
					innerJoin+="where t1.medicine='"+filters.get("medicine")+"' ";
				}
			}else if(filters.get("type").toString().equals("workout")) {
				category="he.name as category,";
				addSelect="(select c.sys_value from notification_content c where c.resource_id=r.id and (c.sys_field='exercise'))as exercise";
				innerJoin="LEFT JOIN ht_exercise hm on hm.id=t1.exercise::int "
						+ "LEFT JOIN ht_exercise_category he on he.id= hm.category_id ";
				if(filters.has("exercise")) {
					innerJoin+="where t1.exercise='"+filters.get("exercise")+" '";
				}
			}
			
			if(filters.has("date")) {
				if(!innerJoin.contains("where")) {
					where="where t1.date='"+filters.get("date")+"' ";
				}else {
					innerJoin+=" and t1.date='"+filters.get("date")+"' ";
				}
			}
			
		
			if(filters.has("status")) {
				if(!innerJoin.contains("where")) {
					if(where.isEmpty()) {
						where="where t1.status='"+filters.get("status")+"' ";
					}else {
						where+=" and t1.status='"+filters.get("status")+"' ";
					}
				}else {
					innerJoin+=" and t1.status='"+filters.get("status")+"' ";
				}
			}
			
			String query = "SELECT t1.*," +((innerJoin.isEmpty()) ? "" : "hm.name as NAME," )+ ((category.isEmpty())? "" : category)+"(SELECT json_agg(nc) from notification_content as nc where nc.resource_id= t1.id ) as CONTENT "
					+ "from (SELECT r.*,(select c.sys_value from notification_content c where c.resource_id=r.id and (c.sys_field ='gid'))as gid,"
					+ "(select c.sys_value from notification_content c where c.resource_id=r.id and (c.sys_field ='date'))as date,"
					+ "(select c.sys_value from notification_content c where c.resource_id=r.id and (c.sys_field ='status'))as status,"
					+ addSelect
					+ " from notification_resource r where r.owner='"+filters.get("patient_id")+"' and r.type='"+filters.get("type")+"') as t1 "
					+ innerJoin + where +" ORDER BY gid,date ASC";
			return query;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
		
	}
	
	public ObjectNode getMedicineHistory(String params) {
		
		String sql = generateHistoricalQuery(params);
		System.err.println(sql);
		if(sql != null) {
			List<JSONObject> response = getJdbcTemplate().query(sql, new Object[] {}, toJsonMapper(NotificationFilter.getMap()));
			
			ObjectNode node = objectMapper.createObjectNode();
			
			response.forEach(json -> {
				
				if(node.has(json.getString("gid"))) {
					ObjectNode dateData = (ObjectNode) node.get(json.getString("gid"));
					if(dateData.has(json.getString("date"))) {
						ArrayNode datas = (ArrayNode) dateData.get(json.getString("date"));
						ObjectNode data = generateContent(json);
						datas.add(data);

						dateData.set(json.getString("date"), datas);
						
						node.set(json.getString("gid"), dateData);
						
					}else {
						ArrayNode datas = objectMapper.createArrayNode();
						ObjectNode data = generateContent(json);
						datas.add(data);
						
						dateData.set(json.getString("date"), datas);
						
						node.set(json.getString("gid"), dateData);
					}
				}else {
					ArrayNode datas = objectMapper.createArrayNode();
					ObjectNode data = generateContent(json);
					datas.add(data);
					
					ObjectNode dateData = objectMapper.createObjectNode();
					dateData.set(json.getString("date"), datas);
					
					node.set(json.getString("gid"), dateData);
				}
			});
			
			
			return node;
		}
		return null;
		
	}
	
	private ObjectNode generateContent(JSONObject json) {
		ObjectNode data = objectMapper.createObjectNode();
		data.put("id",json.get("id").toString());
		data.put("owner",json.get("owner").toString());
		data.put("type",json.get("type").toString());
		data.put("uuid",json.get("uuid").toString());
		data.put("name",json.get("name").toString());
		data.put("category",json.get("category").toString());
		JSONArray arrays = json.getJSONArray("content");
		ArrayNode nodes = objectMapper.createArrayNode();
		arrays.forEach(array -> {
			try {
				JsonNode node = objectMapper.readTree(array.toString());
				if(node.get("sys_field").asText().equals("date_range")) {
					data.set("date_range", node.get("sys_value"));
				}
				nodes.add(node);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		
		data.set("content",nodes);
		
		return data;
		
	}
	

	@Override
	public long count(String filter, Object[] params) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public JSONObject findAll(TableParam params) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String updateNotificationContent(long id,Command command,String dateOfReschedule,boolean isUpdateAll) {
		Optional<NotificationResource> optional = notificationResourceRepository.findById(id);
		if(optional.isPresent()) {
			try {
				NotificationResource resource = optional.get();
				List<NotificationContent> contents = null;
				switch (command) {
					case RESCHEDULE:
						contents = notificationContentRepository.findByResource(resource);
						if(contents != null) {
							contents.forEach(content->{
								if(content.getSysField().equals("time")) {
									content.setSysValue(dateOfReschedule);
								}else if(content.getSysField().equals("status")) {
									content.setSysValue(command.getLabel());
								}
								
							});
							
							update(contents);
						}
						break;
					case UPDATE:
						System.err.println(isUpdateAll);
						if(isUpdateAll) {
							updateAllSameDay(resource.getUuid(), dateOfReschedule);
						}else {
							contents = notificationContentRepository.findByResource(resource);
							if(contents != null) {
								contents.forEach(content->{
									if(content.getSysField().equals("time")) {
										content.setSysValue(dateOfReschedule);
									}
									
								});
								
								update(contents);
							}
						}
						
						break;
					default:
						contents = notificationContentRepository.findByResource(resource);
						if(contents != null) {
							contents.forEach(content->{
								if(content.getSysField().equals("status")) {
									content.setSysValue(command.getLabel());
								}
								
							});
							
							update(contents);
						}
						break;
				}
				
				return "OK";
			}catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		
		return "ERROR";
		
	}
	
	private void updateAllSameDay(String uuid,String time) {
		List<NotificationResource> resources = notificationResourceRepository.findByUuid(uuid);
		if(resources != null) {
			List<NotificationContent> notificationContents = new ArrayList<>();
			resources.forEach(resource -> {
				List<JSONObject> contents = getAllActiveWorkOutByResourceId(resource.getId());
				if(contents != null) {
					
					contents.forEach(content -> {
						try {
							
							List<Content> objects =  objectMapper.readValue(content.get("content").toString(), new TypeReference<List<Content>>(){});
							objects.forEach(object -> {
								NotificationContent notificationContent = new NotificationContent(object.getSys_field(),object.getSys_value(),resource);
								notificationContent.setId(object.getId());
								
								if(notificationContent.getSysField().equals("time")) {
									notificationContent.setSysValue(time);
								}
								notificationContents.add(notificationContent);
							});
							
							
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
					
					
				}
				
			});
			
			System.err.println(notificationContents);
			
			if(!notificationContents.isEmpty()) {
				notificationContentRepository.saveAll(notificationContents);
			}
		}
	}
	
	private List<JSONObject> getAllActiveWorkOutByResourceId(long resourceId){
		String sql="SELECT *,\n" + 
				"				(SELECT json_agg(nc) FROM notification_content nc where resource_id = t1.id) as content \n" + 
				"				FROM (\n" + 
				"				SELECT r.*, \n" + 
				"					(SELECT c.sys_value from notification_content c where c.resource_id = r.id and (c.sys_field = 'status')) as status\n" + 
				"						FROM notification_resource r \n" + 
				"					where  r.id= ?\n" + 
				"				) as t1 where t1.status='ACTIVE' ";
		return  getJdbcTemplate().query(sql, new Object[] {resourceId}, toJsonMapper(NotificationFilter.getMap()));
	}
	
}
