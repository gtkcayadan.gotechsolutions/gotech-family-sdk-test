Required Spring Boot Project.


# gotech-family-sdk

```
<parent>
	<groupId>net.gotech.family</groupId>
	<artifactId>family</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<relativePath/>
</parent>
```


####cloud-login
```
<dependency>
	<groupId>net.gotech.family</groupId>
	<artifactId>family-login</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

Uses cloud-sdk 

```
net.gotech.family.cloud.sdk.jwt.AuthenticationRequest

public ResponseEntity<JsonNode> getToken(JsonNode node) throws AccessDeniedException, IOException, InvalidJwtPathException {
	...
}

```

How to use?

overload class `net.gotech.family.login.service.FamilyLoginServiceImpl.authenticate(JsonNode jsonNode)`, do your user validation from your database or api and response the required json field format.

```
{
  "id": long, // requried
  "username": String, // requried
  "type" : String, // requried
  "obj" : Map //optional
}
```

Allow `/Api/Login` endpoint in your application

```
Override
protected void configure(HttpSecurity http) throws Exception {
	http
	.csrf().disable()
	.authorizeRequests()
	.antMatchers("/Api/Login").permitAll();
}
```


####table-filter
```
<dependency>
	<groupId>net.gotech.family</groupId>
	<artifactId>table-filter</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

####table-dynamic
```
<dependency>
	<groupId>net.gotech.family</groupId>
	<artifactId>table-dyanamic</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

####cloud-sdk

```
<dependency>
	<groupId>net.gotech.family</groupId>
	<artifactId>cloud-sdk</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```